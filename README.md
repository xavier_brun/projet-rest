Projet REST  
Fait par Xavier Brun et Léonard Delêtre
-----

Ce projet scolaire vise à réaliser une single page application (SPA) permettant à l'utilisateur de consulter, d'éditer et d'ajouter des animaux à une simple encyclopédie. Celle-ci consiste en un fichier json que l'utilisateur accède grâce au module json-server de node.
INFO : l'attribut 'chemin' de chaque animal dans animaux.json stocke l'image en base64. Cela explique la taille importante du fichier.


Environnement d'installation
-----
  - Forker et/ou cloner le projet en local
  - Installer json-server
  - Lancer json-server sur le fichier animaux.json
  - Ouvrer le fichier index.html dans un navigateur

Exemple de processus
-----
  - git clone https://gitlab.com/xavier_brun/projet-rest.git
  - cd projet-rest (racine du projet)
  - yarn add json-server
  - ./node_modules/json-server/lib/cli/bin.js animaux.json
  - ouvrer le fichier index.html dans un navigateur

**Vous pouvez maintenant naviguer sur le site**

Fonctionnalités disponibles
-----
  - affichage des animaux contenu dans animaux.json
  - ajout d'animal
  - édition d'animal
  - suppression d'animal
  - ajout de commentaire sur un animal
  - pagination (limite de carte par page = 6 avant de passer à la suivante)

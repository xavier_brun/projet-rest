$(function () {

    // Initialisation des variables globales pour la pagination
    let current_page = 1;
    let cartes_par_page = 6;
    // justSaved indique à la pagination qu'un animal vient d'être ajouté
    // Il faut donc aller vers la dernière page pour montrer l'animal créé
    let justSaved = false;

    // Appel de refreshAnimalList dès le début pour que la page soit initialisée avec les animaux
    refreshAnimalList();

    // Affectation de la fonction refreshAnimalList au bouton "Rafraichir"
    $("#btn_refresh").on("click", function () {
        refreshAnimalList()
    });


    // ----PAGINATION----

    function changerPage(page) {
        // Cette fonction modifie la variable globale qui indique la page actuelle
        // puis rafraichi la liste des animaux (pour effectuer le changement de page)
        current_page = page;
        refreshAnimalList();
    }

    function nbPages(animaux){
        // Return le nombre de page nécessaire en fonctions du nombre d'animaux dans le json et du nombre de carte par page choisi
        return Math.ceil(animaux.length / cartes_par_page);
    }

    function afficherPagination(nbPage, page){
        // Gestion de l'outil de pagination sur la page

        // Récupération de la balise ul contenant l'outil de pagination (boutons) de la page
        let ulPagination = $('#ulPagination');

        // Gestion du bouton pour retourner à la page précédente
        // Si l'utilisateur est à la première page, alors le bouton page précedente est disabled
        let first_page_indicator = "";
        if (page === 1){
            first_page_indicator = "disabled";
        }

        ulPagination.append('<li class="page-item '+first_page_indicator+'">\n' +
            '                <a class="page-link" id="toPagePrevious" href="#">&laquo;</a>\n' +
            '            </li>');

        // Affectation au bouton de la fonction changerPage pour aller à la page précedente
        $('#toPagePrevious').on("click", function () {
            changerPage(page-1);
        });

        // Gestion des boutons de page numérotés
        for (let count = 0; count < nbPage;count++){
            let toPageId = 'toPage' + (count+1);

            // Le bouton de la page actuel est le seul avec la classe "active" (différent affichage)
            let actual_page_indicator = "";
            if (count+1 === page){
                actual_page_indicator = "active";
            }

            ulPagination.append('<li class="page-item '+actual_page_indicator+'">\n' +
                        '            <a class="page-link" id="' + toPageId + '" href="#">'+(count+1)+'</a>\n' +
                        '       </li>');

        // Affectation aux boutons de la fonction changerPage pour aller aux pages indiquées par la boucle
            $('#'+toPageId).on("click", function () {
                changerPage(count+1);
            });
        }

        // Gestion du bouton pour aller à la page suivante
        // Si l'utilisateur est à la denière page, alors le bouton page suivante est disabled
        let last_page_indicator = "";
        if (page === nbPage){
            last_page_indicator = "disabled";
        }

        ulPagination.append('<li class="page-item '+last_page_indicator+'">\n' +
            '                <a class="page-link" id="toPageNext" href="#">&raquo;</a>\n' +
            '            </li>');

        // Affectation au bouton de la fonction changerPage pour aller à la page suivante
        $('#toPageNext').on("click", function () {
                changerPage(page+1);
            });
    }


    // ----AFFICHAGE ANIMAUX----

    function afficherCartes(animaux,page=1) {
        // Cette fonction prend en paramètre les animaux et le numéro de la page.
        // Elle ajoute à l'html les cartes des animaux

        // Actualisation de la variable globale
        current_page = page;

        // arrOptions est une liste utilisée dans la boucle for pour l'affectation des onclicks aux boutons
        let arrOptions = [];

        // Validation de la page

        // Si par erreur le numéro de page passe en dessous de 1, on le remet à 1
        if (current_page < 1) current_page = 1;

        // Si par erreur le numéro de page passe au dessus du nombre de page
        // Ou si l'utilisateur vient d'ajouter un animal, on le remet à la dernière page
        let lastPage = nbPages(animaux);
        if (current_page > lastPage || justSaved){
            current_page = lastPage;
            justSaved = false;
        }

        // Récupération du numéro de la dernière carte de la page actuelle (ou nombre total de carte)
        // Exemple : page 2 -> 2 x 6 (cartes_par_page) = 12
        let numMaxCarte;
        if (current_page * cartes_par_page < animaux.length){
           numMaxCarte = current_page * cartes_par_page;
        }
        else {
            numMaxCarte = animaux.length;
        }

        // Appel de afficherPagination avec les bons paramètres pour l'affichage de l'outil de pagination
        afficherPagination(nbPages(animaux),current_page);

        // Boucle sur une partie des animaux récupérés en paramètre (car les pages ont une limite de carte)
        // Cette boucle ajout les cartes à la div 'animalsContainer' et affecte les onclick
        for (let i = (current_page - 1) * cartes_par_page; i < numMaxCarte; i++) {
            let id_btn_del_animal = "btn_del_animal" + animaux[i].id;
            $('#animalsContainer').append($(
                '<div class="col" style="height: 34em">' +
                '<div class="card shadow-sm h-100">' +
                    '<div id="img_card">' +
                        '<button type="button" id="' + id_btn_del_animal + '" class="close btn_del_animal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        '<img aria-label="Placeholder: Thumbnail" class="bd-placeholder-img card-img-top" ' +
                        'height="225" width="100%" src="' + animaux[i].chemin + '" alt="card_img">' +
                    '</div>' +
                    '<div class="card-body">' +
                        '<p class="card-text font-weight-bold">' + animaux[i].nom + '</p>' +
                        '<p class="card-text" style="height: 50%">' + animaux[i].description+'</p>' +
                        '<div class="d-flex justify-content-between align-items-center">' +
                            '<div class="btn-group">' +
                                '<button class="btn btn-sm btn-outline-info" data-target="#popupAnimal" data-toggle="modal" id="toggleDetailsAnimal'+animaux[i].id+'" type="button">Détails</button>' +
                            '</div>' +
                            '<small id="animal'+animaux[i].id+'commentaires" data-target="#popupAnimal" data-toggle="modal" type="button" class="badge badge-pill badge-success">0 commentaire(s)</small>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '</div>')
            );

            ajouterNbrCommentaires(animaux[i]);

            // Ajout de l'animal bouclé à la liste arrOptions à l'indice correspondant à l'id de l'animal
            arrOptions[animaux[i].id] = animaux[i];
            /*
            Chacune des fonctions qui suivent utilisent l'argument opt,
            on applique (...)(arrOptions[animaux[i].id]) aux fonctions.
            opt prend la valeur de arrOptions[animaux[i].id] (ie, un objet Animal)
            quand on clique sur ces boutons les fcts respectives sont appelées avec les bons paramètres.
            Le but est de faire en sorte que les onclick de chaque carte diffèrent
             */
            document.getElementById("btn_del_animal" + animaux[i].id).onclick = (function (opt) {
                return function () {
                    delAnimal(opt);
                };
            })(arrOptions[animaux[i].id]);

            document.getElementById("toggleDetailsAnimal" + animaux[i].id).onclick = (function (opt) {
                return function () {
                    afficherAnimal(opt);
                };

            })(arrOptions[animaux[i].id]);

            document.getElementById("animal"+animaux[i].id+"commentaires").onclick = (function (opt) {
                return function () {
                    afficherCommentaires(opt);
                };
            })(arrOptions[animaux[i].id]);

        }
    }

    function ajouterNbrCommentaires(animal) {
        // Cette fonction ajoute le nombre de commentaire en bas à droite de chaque carte
        let url = "http://localhost:3000/commentaires?animalId=" + animal.id;
        fetch(url)
            .then((response) => response.json())
            .then(function (data) {
                document.getElementById("animal"+animal.id+"commentaires").textContent=data.length + " commentaires(s)";
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    // Cette variable globale permet à la fonction saveModifiedAnimal de savoir sur quel animal modifier les informations
    let editedAnimalId = 0;
    function afficherAnimal(animal){
        // Cette fonction appelle les fonctions nécessaires pour remplir la popup d'un animal en mode "vu" (contraire du mode "edition")
        editedAnimalId = animal.id;
        formAnimal("vu");
        fillFormAnimal(animal);
        ajoutBoutons("vu");
    }


    // Cette variable globale permet à la fonction saveNewCommentaire de savoir quel animal est commenté
    let commentedAnimal;

    function afficherCommentaires(animal) {
        // Cette fonction remplit la popup "Commentaire" de l'animal passé en paramètre


        //pour que le formulaire d'ajout de commentaire ait accès à l'animal commenté
        commentedAnimal = animal;

        // Récupération des balises div de la popup
        let $modalheader = $("#popupBtnHeader");
        let $popupTitle = $("#popupTitle");
        let $popupBody = $("#popupBody");
        let $modalfooter = $("#modal-footer");

        // Suppression du contenu des divs
        $modalheader.empty();
        $popupTitle.empty();
        $popupBody.empty();
        $modalfooter.empty();

        // Remplissage des différentes parties de la popup avec les informations commentaires
        $popupTitle.append($('<h5 id="titrepopup" class="modal-title">Commentaires sur ' + animal.nom + '</h5>'));
        $popupBody.append('<div id="divListeCommentaires">');
        placerCommentaires(animal);
        $popupBody.append('</div>');
        $popupBody.append('<div id="divFormCommentaire" class="mt-4">');

        // Ajout du formulaire d'ajout de commentaire
        $("#divFormCommentaire")
            .append(createTextInput("auteurInput", "Auteur", "edition"))
            .append(createTextInput("contenuInput", "Ajouter un commentaire", "edition"));

        $modalfooter.append('<button id="boutonAnnuler" class="btn btn-secondary" data-dismiss="modal" type="button">Retour</button>');
        $modalfooter.append('<button class="btn btn-success" id="addCommentaireBtn" type="button">Ajouter un commentaire</button>');
        $("#addCommentaireBtn").on("click", function () {
                // vérification que le formulaire est totalement rempli
                let isIncomplet = gererAlerteFormulaire();
                if (!isIncomplet) {
                    saveNewCommentaire();
                }
        });
    }

    function placerCommentaires(animal){
        // Cette fonction utilise le protocol GET d'ajax pour récupérer et ajouter les commentaires au html
        let url = "http://localhost:3000/commentaires?animalId=" + animal.id;

        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            success: function (commentaires) {
                let $divListeCommentaires = $("#divListeCommentaires");
                for (let i=0;i<commentaires.length;i++) {
                    $divListeCommentaires
                        .append('<div class="card border-primary mb-3">\n' +
                            '  <div class="card-header">'+commentaires[i].auteur+'</div>\n' +
                            '  <div class="card-body">\n' +
                            '    <p class="card-text">'+commentaires[i].contenu+'</p>\n' +
                            '  </div>\n' +
                            '</div>'
                    );
                }
            },
            error: function (req, status, err) {
                console.log("Erreur lors de la récupération des commentaires");
            }
        });
    }

    function onerror(err) {
        $("#champs").html("<b>Impossible de récupérer les champs à réaliser !</b>" + err);
    }

    function refreshAnimalList() {
        // Cette fonction retire toutes les cartes (et l'outil de pagination)
        // puis appelle afficherCartes avec les animaux récupérés en fetch
        $("#animalsContainer").empty();
        $("#ulPagination").empty();

        let requete = "http://localhost:3000/animaux";
        fetch(requete)
            .then(response => {
                    if (response.ok) return response.json();
                    else throw new Error('Problème ajax: ' + response.status);
                }
            )
            .then(function (data) {
                afficherCartes(data,current_page);
            })
            .catch(onerror);
    }

    // création des objets utilisés pour l'ajout, l'edition et l'affichage des informations du json
    class Animal {
        constructor(nom, nom_scientifique, esperance_de_vie, taille, poids, famille, description, chemin) {
            this.nom = nom;
            this.nom_scientifique = nom_scientifique;
            this.esperance_de_vie = esperance_de_vie;
            this.taille = taille;
            this.poids = poids;
            this.famille = famille;
            this.description = description;
            this.chemin = chemin;
        }
    }

    class Commentaire {
        constructor(auteur, contenu, animalId) {
            this.auteur = auteur;
            this.contenu = contenu;
            this.animalId = animalId;
        }
    }

    // Ajout de fonctions appelés par le onclick du bouton "Ajouter un animal"
    $("#toggleAddAnimal").on("click", function () {
        formAnimal("ajout");
        ajoutBoutons("ajout");
    });

    function readURL(input) {
        // Cette fonction récupère une image rentrée dans un file explorer, la convertie en base64 et l'affiche sur la balise 'img_output'
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                document.getElementById('img_output').src = e.target.result
            }

            reader.readAsDataURL(input.files[0]); // conversion en base64 string
        }
    }

    function formAnimal(type) {
        // Cette fonction prend en paramètre le type de formulaire (consultation/édition/ajout)
        // Création du formulaire de création/édition d'animal sur la popup
        // Cette fonction est aussi utilisée pour la simple consultation des informations d'un animal sur la popup, les inputs sont alors en readonly
        let $popupTitle = $("#popupTitle");
        let $popupBody = $("#popupBody");

        $popupTitle.empty();

        // Ajout du titre
        if (type==="ajout") {
            $popupTitle.append($('<h5 id="titrepopup" class="modal-title">Ajouter un animal</h5>'));
        } else if (type==="edition") {
            $popupTitle.append($('<h5 id="titrepopup" class="modal-title">Editer un animal</h5>'));
        } else {
            $popupTitle.append($('<h5 id="titrepopup" class="modal-title">Informations</h5>'));
        }

        $popupBody.empty();

        // Ajout des inputs du formulaire
        $popupBody
            .append(createTextInput('nom', 'Nom', type))
            .append(createTextInput('nom_scientifique', 'Nom scientifique', type))
            .append(createNumberInput('esperance_de_vie', 'Esperance de vie en année', type))
            .append(createNumberInput('taille', 'Taille en cm', type, 0.5))
            .append(createNumberInput('poids', 'Poids en kg', type, 0.5,))
            .append(createTextInput('famille', 'Famille', type))
            .append(createTextArea('description', 'Courte description de l\'animal', type))
            .append(type==="vu" ? $('<div hidden id="imageDownloader" class="form-group">\n' +
                '<div class="custom-file">' +
                '<input type="file" class="custom-file-input" accept=".jpg, .jpeg, .png" id="chemin">\n' +
                '<label class="custom-file-label" for="chemin">Sélectionner une image</label>' +
                '</div>' +
                '</div>')
                : $('<div class="form-group">\n' +
                '<div class="custom-file">' +
                '<input type="file" class="custom-file-input" accept=".jpg, .jpeg, .png" id="chemin">\n' +
                '<label class="custom-file-label" for="chemin">Sélectionner une image</label>' +
                '</div>' +
                '</div>'))
 
            .append($('<img id="img_output" class="img-fluid" src="img/animal_default.jpg"  alt="your image" />'));

        // Ajout de l'appel à readURL lorsque l'utilisateur entre un fichier dans l'input file
        $("#chemin").on("change", function () {
            readURL(this)
        });
    }

    function editableForm(){
        // Cette fonction transforme un formulaire de consultation (=readonly) en formulaire d'édition
        // Cette fonction est appelée par le bouton 'Editer' en haut d'une popup

        for(let child of document.getElementsByTagName("input")){
            if(child.hasAttribute("readonly")){
                child.removeAttribute("readonly");
            }
        }

        // concaténation des listes de bouttons
        let listeBtnMoins = Array.prototype.slice.call(document.getElementsByClassName("btn btn-minus plus_minus_btn"), 0);
        let listeBtnPlus = Array.prototype.slice.call(document.getElementsByClassName("btn btn-plus plus_minus_btn"), 0);
        let listeBtn = listeBtnMoins.concat(listeBtnPlus);

        for(let child of listeBtn){
            if(child.hasAttribute("disabled")){
                child.removeAttribute("disabled");
            }
        }

        document.getElementById('imageDownloader').removeAttribute("hidden");
        document.getElementById('description').removeAttribute("readonly");
        document.getElementById('titrepopup').innerHTML = "Éditer un animal";
    }

    function readableForm(){
        // Cette fonction transforme un formulaire d'édition en formulaire de consultation (en mettant les inputs en readonly)
        // Cette fonction est appelée par le bouton 'Voir' en haut d'une popup

        for(let child of document.getElementsByTagName("input")){
            if(!child.hasAttribute("readonly")){
                child.setAttribute("readonly", "");
            }
        }

        // concaténation des listes de bouttons
        let listeBtnMoins = Array.prototype.slice.call(document.getElementsByClassName("btn btn-minus plus_minus_btn"), 0);
        let listeBtnPlus = Array.prototype.slice.call(document.getElementsByClassName("btn btn-plus plus_minus_btn"), 0);
        let listeBtn = listeBtnMoins.concat(listeBtnPlus);

        for(let child of listeBtn){
            if(!child.hasAttribute("disabled")){
                child.setAttribute("disabled", "");
            }
        }

        document.getElementById('imageDownloader').setAttribute("hidden", "");
        document.getElementById('description').setAttribute("readonly", "");
        document.getElementById('titrepopup').innerHTML = "Informations";
    }

    function ajoutBoutons(type) {
        // Ajoute les boutons en bas de la popup en fonction du type de celle-ci

        let modalheader = $("#popupBtnHeader");
        modalheader.empty();

        // L'aspect des boutons en haut de la popup pour changer de type change en fonction du type actuelle
        let voirActive = "";
        let editActive = "";
        if (type === "vu") voirActive = "active";
        if (type === "edition") editActive = "active";

        if (type === "vu" || type === "edition") {
            modalheader.append($('<button class="btn btn-outline-success '+editActive+'" id="editionAnimalBtn" type="button">Éditer</button>').on("click", function () {
                editableForm();
                ajoutBoutons("edition");
            }));

            modalheader.append($('<button class="btn btn-outline-info '+voirActive+'" id="editionAnimalBtn" type="button">Voir</button>').on("click", function () {
                readableForm();
                ajoutBoutons("vu");
            }));
        }


        // Ajout des boutons de confirmation et d'annulation de formulaire
        let modalfooter = $("#modal-footer");
        modalfooter.empty();
        modalfooter.append(type === "vu" ? $('<button id="boutonAnnuler" class="btn btn-secondary" data-dismiss="modal" type="button">Retour</button>')
            : $('<button id="boutonAnnuler" class="btn btn-secondary" data-dismiss="modal" onclick="$(\'#popupAnimal\').modal(\'hide\')" type="button">Annuler</button>'));

        modalfooter.append(type === "ajout" ? $('<button class="btn btn-success" id="addAnimalBtn" type="button">Confirmer</button>').on("click", function () {
                // Gestion du message d'alerte si formulaire incomplet
                let isIncomplet = gererAlerteFormulaire();

                if (!isIncomplet) {
                    // Si le formulaire est bien rempli, la popup d'ajout se ferme et l'animal est ajouté
                    $('#popupAnimal').modal('hide');
                    saveNewAnimal();
                }
            })
            : type === "edition" ? $('<button class="btn btn-primary" id="editAnimalBtn" type="button">Editer</button>').on("click", function () {
                    // Gestion du message d'alerte si formulaire incomplet
                    let isIncomplet = gererAlerteFormulaire();

                    if (!isIncomplet) {
                        // Si le formulaire est bien rempli, l'animal est edité et la popup passe en mode consultation
                        saveModifiedAnimal();
                        readableForm();
                        ajoutBoutons("vu");
                    }
                })
                : ''
        )
    }

    function gererAlerteFormulaire(){
        // Gestion du message d'alerte si formulaire (ajout/edit) incomplet
        // Cette fonction renvoie true si le formulaire est incomplet

        let isIncomplet = false;

        // Boucle sur toutes les champs du formulaire
        for (let child of document.getElementsByClassName("form-control")) {

            // Si le champs est vide (ou seulement rempli d'espace)
            if (child.value.trim() === "") {
                isIncomplet = true;

                // Si le message d'alerte n'est pas déjà affiché
                if (!document.getElementById('alertIncomplet')) {
                    $("#popupBody")
                        .append('<div id="alertIncomplet" class="alert alert-dismissible alert-danger">\n' +
                            '  <strong>Attention !</strong> Vous n\'avez pas rempli tous les champs' +
                            '</div>');
                }

                // On sort de la boucle for car on sait déjà que le formulaire est incomplet
                break;
            }
        }

        return isIncomplet;
    }

    function createTextArea(textarea_id, placeholder, type){
        // Création d'une balise textarea avec un id, un placeholder et un type (readonly ou non)

        let readonly = "";
        if(type==="vu"){
            readonly = "readonly";
        }

        return '<div class="form-group">' +
        '<label for=' + textarea_id + ' class="form-label">' + placeholder + '</label>' +
        '<textarea maxlength="120" '+readonly+' class="form-control" id="'+textarea_id+'" placeholder="'+placeholder+'" rows="3"></textarea>' +
        '</div>';

    }

    function createTextInput(input_id, input_placeholder, type) {
        // Création d'une balise input texte avec un id, un placeholder et un type (readonly ou non)

        let readonly = "";
        if(type==="vu"){
            readonly = "readonly";
        }

        return '<div class="form-group">\n' +
            '<label for=' + input_id + ' class="form-label">' + input_placeholder + '</label>' +
            '<input '+readonly+' type="text" class="form-control" id="' + input_id + '" placeholder="' + input_placeholder + '">\n' +
            '</div>';

    }

    function createNumberInput(input_id, input_label, type, input_step = 1) {
        // Création d'une balise input number personnalisée (entouré de boutons + et -)
        // avec un id, un label associé, un placeholder, un type (readonly ou non) et un step (ex : de 1 en 1)

        let readonly = "";
        let disabled = "";
        if(type==="vu"){
            readonly = "readonly";
            disabled = "disabled";
        }


        return '<div class="form-group">\n' +
        '<label for=' + input_id + ' class="form-label">' + input_label + '</label>' +
        '<div id="div_number_input" class="input-group">\n' +
        '  <div class="input-group-prepend">\n' +
        '    <button '+disabled+' class="btn btn-minus plus_minus_btn" id="btnmoins'+input_id+'" onclick="this.parentNode.parentNode.querySelector(\'#' + input_id + '\').stepDown()">\n' +
        '      <i class="fa fa-minus"></i>\n' +
        '    </button>\n' +
        '  </div>\n' +
        '  <input '+readonly+' class="quantity number_input text-center" min="0" id="' + input_id + '" step="' + input_step + '" name="quantity" value="1" type="number">\n' +
        '  <div class="input-group-append">\n' +
        '    <button '+disabled+' class="btn btn-plus plus_minus_btn" id="btnplus'+input_id+'" onclick="this.parentNode.parentNode.querySelector(\'#' + input_id + '\').stepUp()">\n' +
        '      <i class="fa fa-plus"></i>\n' +
        '    </button>\n' +
        '  </div>\n' +
        '</div>' +
        '</div>';

    }

    function fillFormAnimal(animal) {
        // Remplissage des champs du formulaire (consultation/édition) avec les informations de l'animal

        $("#popupBody #nom").val(animal.nom);
        $("#popupBody #nom_scientifique").val(animal.nom_scientifique);
        $("#popupBody #esperance_de_vie").val(animal.esperance_de_vie);
        $("#popupBody #taille").val(animal.taille);
        $("#popupBody #poids").val(animal.poids);
        $("#popupBody #famille").val(animal.famille);
        $("#popupBody #description").val(animal.description);
        document.getElementById("img_output").src=animal.chemin;
    }


    function saveNewAnimal() {
        // Enregistre un nouvel animal sur la BD (json-server) puis rafraichis la liste des animaux

        // Création de l'objet Animal grâce aux valeurs de chaque champs du formulaire
        let animal = new Animal(
            $("#popupBody #nom").val(),
            $("#popupBody #nom_scientifique").val(),
            $("#popupBody #esperance_de_vie").val(),
            $("#popupBody #taille").val(),
            $("#popupBody #poids").val(),
            $("#popupBody #famille").val(),
            $("#popupBody #description").val(),
            document.getElementById('img_output').src
        );

        // Insertion en BD grâce à la méthode 'POST'
        fetch("http://localhost:3000/animaux/", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(animal)
        })
            .then(res => {
                console.log('Save Success');
                $("#result").text(res['contenu']);

                // Mise à jour de la variable globale justSaved
                justSaved = true;
                // Rafraichissement de la liste des animaux
                refreshAnimalList();
            })
            .catch(res => {
                console.log(res)
            });
    }

    function saveNewCommentaire() {
        // Enregistre un nouveau commentaire sur la BD (json-server) puis rafraichis la liste des animaux et la liste des commentaires de l'animal

        // Création de l'objet Commentaire grâce aux valeurs de chaque champs du formulaire
        // L'id de l'animal commenté est récuperé grâce à la variable globale 'commentedAnimal'
        let commentaire = new Commentaire(
            $("#divFormCommentaire #auteurInput").val(),
            $("#divFormCommentaire #contenuInput").val(),
            commentedAnimal.id
        );

        // Insertion en BD grâce à la méthode 'POST'
        fetch("http://localhost:3000/commentaires/", {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(commentaire)
        })
            .then(res => {
                // Rafraichissement de la liste des animaux
                refreshAnimalList();

                // Rafraichissement de la liste des commentaires sur 'commentedAnimal'
                $("#divListeCommentaires").empty();
                placerCommentaires(commentedAnimal);
            })
            .catch(res => {
                console.log(res)
            });
    }

    function saveModifiedAnimal() {
        // Edite un animal sur la BD (json-server) puis rafraichis la liste des animaux

        // Création de l'objet Animal grâce aux valeurs de chaque champs du formulaire
        // L'id de l'animal edité est récuperé grâce à la variable globale 'editedAnimalId'
        let uri = 'http://localhost:3000/animaux/' + editedAnimalId;
        let animal = new Animal(
            $("#popupBody #nom").val(),
            $("#popupBody #nom_scientifique").val(),
            $("#popupBody #esperance_de_vie").val(),
            $("#popupBody #taille").val(),
            $("#popupBody #poids").val(),
            $("#popupBody #famille").val(),
            $("#popupBody #description").val(),
            document.getElementById('img_output').src

        );

        // Edition en BD grâce à la méthode 'PUT'
        fetch(uri, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "PUT",
            body: JSON.stringify(animal)
        })
            .then(res => {
                console.log('Update Success');
                // Rafraichissement de la liste des animaux
                refreshAnimalList();
            })
            .catch(res => {
                console.log(res)
            });
    }

    function delAnimal(animal) {
        // Supprime un animal sur la BD (json-server) puis rafraichis la liste des animaux

        // Récupération de l'url menant à l'animal
        let url = "http://localhost:3000/animaux/" + animal.id;

        // Edition en BD grâce à la méthode 'DELETE'
        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "DELETE"
        })
            .then(res => {
                console.log('Delete Success:' + res);
            })
            .then(refreshAnimalList)
            .catch(res => {
                console.log(res);
            });
    }
});
